# Examples

**NB: The API has changed so that some methods can only be accessed using GET requests, whilst private methods still require POST. Most of these examples still use POST for all requests, and this is liable to fail on some public methods. There is a python2 example with both get and post [here](https://bitbucket.org/nitrous/mtgox-api/overview#markdown-header-security), and the api3.py below is a python3 example with both get and post, so hopefully you should be able to see how to extend the other examples to include GET requests.**

This directory will hopefully grow to contain examples of using the API in a variety of languages. Feel free to send in any source code you have constructed :) A list of the current implementations is below:

File | Language | Author | Description
--- | --- | --- | ---
api.py | Python2 | nitrous | A quick and basic class I made for use in my simple MtGox ticker example (<https://bitbucket.org/nitrous/simple-mtgox-ticker>).
api3.py | Python3 | zhangweiwu | A rewrite of the python2 [example](https://bitbucket.org/nitrous/mtgox-api/overview#markdown-header-security) in python3 using new modules.
api.vb | VB.Net | nitrous | An implementation I made on request to demonstrate a working example.
api.node.js | node.js | ameen3 | Sent in by Ameen, the main project is here (<https://github.com/ameen3/node-mtgox-apiv2>). Dependencies are `querystring` and `jsSHA`. The version here is the original simple version, however the project has been updated to have a friendly interface to many API methods.
 | [Java](https://github.com/adv0r/mtgox-api-v2-java) | advanced | Advanced has prepared a java client to enable easy MtGox trading here (<https://github.com/adv0r/mtgox-api-v2-java>), currently in BTC/USD/EUR. This is far more sophisticated than the other examples, in that it provides a friendly interface to the API via a class through which the API is called, rather than the raw URL methods used for the above. This code is ready to use if you want (although he has put a liability disclaimer!), or you can use it as a template for building your own sophisticated OOP client.
 | [Ruby](https://github.com/renich/mgc) | Renich | Renich has created this Ruby command line tool here (<https://github.com/renich/mgc>) that can take a v2 method and a JSON argument string. Dependencies can be installed by using `bundle`, or manually (`faraday` and `json` gems).